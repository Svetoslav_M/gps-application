package routeDataPoint;

public class RouteDataPoint {
	String latitude;
	String longitude;
	String elevation;
	String speed;
	String time;
	String data;
	String[] dataParts;

	public RouteDataPoint(String s) {
		data = s;
		dataParts = data.split(" ");

		latitude = dataParts[0];
		longitude = dataParts[1];
		elevation = dataParts[2];
		speed = dataParts[3];
		time = dataParts[4];
	}

	public double getElevation() {
		return Double.parseDouble(elevation);
	}

	public double getLat() {
		return Double.parseDouble(latitude);
	}

	public double getLong() {
		return Double.parseDouble(longitude);
	}

	public double getTime() {
		return Double.parseDouble(time);
	}

	public double getDistanceInKm(RouteDataPoint other) {
		double R = 6371; // km
		double dLat = Math.toRadians((other.getLat() - getLat()));
		double dLon = Math.toRadians((other.getLong() - getLong()));
		double lat1 = Math.toRadians(getLat());
		double lat2 = Math.toRadians(other.getLat());
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2)
				* Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double d = R * c;

		return d;

	}

	public String toString() {
		return "" + Double.parseDouble(latitude) + " "
				+ Double.parseDouble(longitude) + " "
				+ Double.parseDouble(elevation) + " "
				+ Double.parseDouble(speed) + " " + Double.parseDouble(time);

	}
}