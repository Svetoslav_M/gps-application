package routeDataPoint;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Route {

	String line;
	String fileRoute;
	ArrayList<RouteDataPoint> routes;

	public Route(String filename) {
		routes = new ArrayList<RouteDataPoint>();
		fileRoute = filename;

		try {
			FileReader routeReader = new FileReader(fileRoute);
			BufferedReader routeBuffer = new BufferedReader(routeReader);

			while ((line = routeBuffer.readLine()) != null) {
				RouteDataPoint rdp = new RouteDataPoint(line);
				routes.add(rdp);
			}
			routeBuffer.close();
		}

		catch (IOException e) {
			System.out.println("FILE NOT FOUND");
		}

	}

	public String getObjects(int n) {

		return routes.get(n) + "";
	}

	public double getRouteLengthInKm() {
		double d = 0;
		for (int i = 0; i < routes.size() - 1; ++i) {
			d = d + routes.get(i).getDistanceInKm(routes.get(i + 1));
		}
		return d;
	}

	public double maxEl() {
		double a = routes.get(0).getElevation();
		for (int i = 0; i < routes.size(); ++i) {
			if (a < routes.get(i).getElevation()) {
				a = routes.get(i + 1).getElevation();
			}
		}
		return a;
	}

	public double minEl() {
		double a = routes.get(0).getElevation();
		for (int i = 0; i < routes.size(); ++i) {
			if (a > routes.get(i).getElevation()) {
				a = routes.get(i).getElevation();
			}
		}
		return a;
	}

	/*
	 * public double time() { double a = routes.get(0).getTime(); for (int i =
	 * 0; i < routes.size(); ++i) { if (a < routes.get(i).getTime()) { a =
	 * routes.get(i).getTime();
	 * 
	 * }
	 * 
	 * } return a;
	 * 
	 * }
	 */

	public double time() {
		double a = 0;
		for (int i = 0; i < routes.size() - 1; ++i) {
			a = a + (routes.get(i + 1).getTime() - routes.get(i).getTime());
		}
		return a;
	}

	public ArrayList<Double> getCorrectedElevation() {
		ArrayList<Double> s = new ArrayList<Double>();
		double a = routes.get(0).getElevation();
		s.add(a);
		double temp = 0;
		for (int i = 0; i < routes.size() - 1; ++i) {
			if (routes.get(i + 1).getElevation() - routes.get(i).getElevation() <= 10) {
				a = routes.get(i + 1).getElevation();
				temp = routes.get(i).getTime();
				s.add(a);

			} else {
				if (routes.get(i).getTime() - temp > 30) {
					s.add(routes.get(i).getElevation());
				}

			}
		}

		return s;
	}

	public String toString() {
		String s = "";
		for (RouteDataPoint rdp : routes) {
			s += rdp + "\n";
		}
		return s;
	}

}