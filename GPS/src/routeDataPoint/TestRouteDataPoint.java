 package routeDataPoint;

import java.util.ArrayList;

public class TestRouteDataPoint {

	public static void main(String args[]) {

		// A few lines of route data
		final String testLines[] = { "51.51026309 -0.11796892 55 2.7 0",
				"51.51022553 -0.11794746 56 8.1 4",
				"51.51020408 -0.117926 55 8.1 5",
				"51.51018262 -0.11791527 56 9 6" };

		// Make space to store the points that are created from this
		// Note the use of an initial capacity (testLines.length) - this is more
		// efficient
		ArrayList<RouteDataPoint> route = new ArrayList<RouteDataPoint>(
				testLines.length);

		for (String line : testLines) {

			// make the point for the next line
			RouteDataPoint a = new RouteDataPoint(line);

			// print out the before and after
			System.out.println("In : " + line);
			System.out.println("Out: " + a);

			// add the point to 'route'
			route.add(a);

		}

		// write your code for 3b here
		double elevation = 0.0;
		double firstEl = route.get(0).getElevation();
		System.out.println(firstEl);
		// elevation = elevation + (route.get(1).getElevation() -
		// route.get(0).getElevation());

		for (int i = 0; i < route.size() - 1; ++i) {
			if (route.get(i).getElevation() > route.get(i + 1).getElevation()) {
				continue;
			}
			elevation = elevation
					+ (route.get(i + 1).getElevation() - route.get(i)
							.getElevation());
		}
		System.out.println(elevation);
	}
}